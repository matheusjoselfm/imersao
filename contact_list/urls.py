from django.urls import path
from .views import register, index, edit, delete

urlpatterns = [
    path(r'register', register, name="register"),
    path(r'index', index, name="index"),
    path(r'edit/<int:pk>', edit, name="edit"),
    path(r'delete/<int:pk>', delete, name="delete"),
]
