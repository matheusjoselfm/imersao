from django.shortcuts import render, redirect, get_object_or_404
from .models import Contact
from .forms import ContactForm
from django.contrib.auth import logout, authenticate, login
from django.http import HttpResponse

def register(request):
    form = ContactForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('index')
    return render(request, 'edit_create.html', {'form':form})

def index(request):
    contato = Contact.objects.all()
    context = {'contatos':contato}
    return render(request, 'index.html', context)

def edit(request, pk):
    contact = get_object_or_404(Contact, pk=pk)
    if request.method == 'POST':
        form = ContactForm(request.POST, instance=contact)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = ContactForm(instance=contact)
    return render(request, 'edit_create.html', {'form':form})

def delete(request, pk):
    contact = Contact.objects.get(pk=pk)
    if request.method == 'POST':
        contact.delete()
        return redirect('index')
    return render(request, 'delete.html', {'contact':contact})
    